---
title: Sous le capot
draft: false
weight: 3
---

## Docker ##

* cela permet de pouvoir générer temporairement les outils qui vont convertir notamment le langage **Markdown** en langage HTML (compris pour tous les navigateurs Internet).

## Gitlab CI ##

* [comprendre la CI Gitlab](https://blog.eleven-labs.com/fr/introduction-gitlab-ci/),
* il va permettre de générer les nouvelles pages HTML suite à vos modifications.
