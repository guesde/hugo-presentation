---
title: "Orange Formation"
---

# {{< param globalTitle >}}

Supports de formation : Laurent Guesde De Gouvea</br>

Conçus initialement dans le cadre d'une présentation Orange Code Room</a>.</br>

Sous [licence CC-BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.fr).</a>

## Pourquoi cette présentation ?

* le but de cette présentation a pour objectif:
  * maintenir un site Internet (thèmes compris),
  * pouvoir facilement créer des articles ou du contenu.

## Pourquoi un site Internet ?

* partager une ou des passions via un blog,
* avoir son CV en ligne,
* avoir des notes accessibles en ligne,
* être autonome tout en pouvant partager sur les différents sociaux.

[Des exemples de sites faits à partir d'Hugo](https://gohugo.io/showcase/).

## Pourquoi Hugo?

* [Hugo](https://gohugo.io/) est un outil simple pour faire des sites Internet,
* il se base sur un [langage de balisage léger](https://fr.wikipedia.org/wiki/Markdown),
* il peut être hébergé gratuitement,
* il est possible de faire des publications ou des modifications de vos articles juste avec un navigateur web comme [Firefox](https://www.mozilla.org/fr-FR/firefox).
