---
title: Prérequis
draft: false
weight: 1
---

# Pour bien commencer...

Nous sommmes le 13 février 2024.


## Une adresse email

* cela vous permettra de gérer son compte Gitlab (perte de mot de passe, etc,...).

## Un compte Gitlab

* pour cela, vous avez le choix entre ces différentes plateformes:
  * [Gitlab](https://gitlab.com),
  * [Framagit](https://framagit.org),
  * [Github](https://github.com).

## ... et surtout de l'imagination

Alors à vous de jouer !!  **;-)**
