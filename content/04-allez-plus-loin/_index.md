---
title: Allez plus loin
draft: false
weight: 4
---

## Gitlab ##

* [un tutoriel Gitlab en français](https://github.com/SocialGouv/tutoriel-gitlab) pour mieux comprendre l'outil Git.

## Markdown ##

* un [aide-memoire du langage](https://github.com/tchapi/markdown-cheatsheet) utilisé sur Hugo pour produire du texte.
